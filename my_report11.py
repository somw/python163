import random
''' 算术运算符
+ 加法运算符
- 减法运算符
* 乘法运算符
/ 除法运算符
** 指数运算符
% 取余运算符，计算余数
// 除法取整运算符，计算商并去除其小数部分
'''
# sum1 = 5
# sum2 = 2
# sum3 = sum1 ** sum2
# print(sum3)

# 比较运算符 = < > <= >= ==(判断两个对象运算是否相同) !=(判断两个对象运算是否不相同)

# name = input('请输入您的姓名：')
# sex = input('请输入您的性别：')
# age = input('请输入您的年龄：')
# school = input('请输入您的大学：')

# print('*' * 30)
# print('简历介绍'.center(26,' '))
# aaa = '请输入您的大学：{:>5s}\t网址：{:s}'
# bbb =aaa.format('23','11')
# print(bbb)


# s='abcdefg'
# [3:]  从左到右，开头是0，排除第0个到第三个，打印出来defg
# [:5]  从左到右，开头是1，取开始到第五个，打印出来abcde
# [-2:] 从右到左，开头是1，取结尾到倒数第二个，打印出来fg
# [:-1] 从右到左，开头是1，排除倒数第一个，打印出来abcef
# list[start:end:step] 起始位置：结束位置：步长
# print(s[:-5])

# x = 'hello world'
# a = x.replace('hello','hi')
# print(a)

# aa  =18
# bb = 17
# print(aa > 0 and bb<0)

# hh = list('haha')
# hh[0] = 'a'
# # print(hh[0]+hh[3])
# print(hh)

# hao = ['小红','小宝']
# print(hao)
# hao.append('大宝')
# print(hao)

# number1 = list('01234115')
# print(number1)
# number1 = [0,1,2,3,4,1,1,5]
# number2 = list('56789')
# print(number1.index(2))

# names =['李雷','王小强','小红']
# phones = ['123','456','789']
# print(phones[names.index('李雷')+1])

# names = {'李雷':123,'王小强':456,'小红':789}
# names['李雷'] = 200
# names['haha'] = 568
# print(names)

# del names['haha']
# print(names)

# # names.clear()
# # print(names)

# del names
# # print(names)
# a = 2
# b = 3
# b //= a
# print(b)

# a = 'hello python'
# news = 'p' + a[1:] 
# print(news)

# print('aa\bbb')

# a = list('python')
# print(a)
# print(a[2:])
# a[2:] = list('abc')
# print(a)


# 分片赋值
# a = [1,5]
# print(a[1:1])

# a[1:1] = [2,3,4]
# print(a)

# a[1:4] =[]
# print(a)

# 字典
# a = [('haha',55),('zzz',33)]
# b = dict(a)
# a = dict(bbb=999)
# print(a)

# 字典列表
# a = {'name':'hong','age':33,'grade':98}
# b = {'name':'xiaobai','age':20,'grade':100}
# c = {'name':'heihei','age':18,'grade':99}
# abc = [a,b,c]
# print(abc)

# 在字典中存储列表
# a = {
# 	'hong':['数学','英语'],
# 	'xiaobai':['计算机'],
# 	'heihei':['数学','英语','语文']
# }
# print(a['hong'])
# print(a['hong'][1])

# 在字典中存储字典
# 用一个字典表示一个学生信息
# student1 = {'name':'hong','age':33,'grade':98}
# # 用一个字典表示全班学生信息
# class1 = {
# 	'hong':{'age':20,'grade':100},
# 	'xiaobai':{'age':33,'grade':98}
# }
# print(class1['xiaobai'])
# print(class1['xiaobai']['age'])

# a = [1,2,3,4,5]
# b = a
# b[1] = 6
# print(a)
# print(b)
# print(f'a:{a}')
# print(f'b:{b}')


# a = [1,2,3,4,5]
# b = a[:]
# b[1] = 6
# print(a)
# print(b)
# print(f'a:{a}')
# print(f'b:{b}')

# 对元祖的取值与切片操作
# a = ('a',22,'dddd')
# print(a)
# print(a[1])
# print(a[0:2])

# 集合
# a = set([1,1,1,2,1,5,6,8,9])
# b = set([2,5,9,7,4])
# print(a)
# print(b)

# c ={1,1,1,2,6,1,2,7}
# d = [1,1,1,2,6,1,2,7]
# print(d)

# 交集&、并集|、差集-、对称差集^
# a = {1,1,1,2,1,5,6,8,9}
# b = {2,5,9,7,4}
# print(a&b)
# print(a|b)
# print(a-b)
# print(a^b)
# c = (a|b)-(a&b)
# print(c)

# x = [1,2,3]
# x.insert(1,4)
# print(x)

# L1 = ['Google', 'Runoob', 'Taobao']
# L1.insert(1, 'Runoob')
# print ('列表插入元素后为 : ', L1)

# a = {1,2,3,2,5,6,2,1}
# print(len(a))
x = []
for i in range(20):
	x.append(random.randint(0,50))
# print(x)
y = x[::2]
# print(y)


x[::2] = y
# print(y)
print(x)