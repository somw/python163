# -*- coding: utf-8 -*-
# @Author: lenovo
# @Date:   2020-11-12 09:24:00
# @Last Modified by:   somw
# @Last Modified time: 2020-11-13 10:43:47

#1
# x = list(range(20))
# for i in range(int(len(x)/2)):
# 	# if i < len(x)/2:
# 	del x[i]
# 	# else:
# 		# break
# print(x)
	#print('未删除之前：{},x列表长度为{},索引下标的值为{}'.format(x[i],len(x),i))
	#print(x)
	
	#print('未删除之后：{},x列表长度为{},索引下标的值为{}'.format(x[i],len(x),i))
	#print(x)


#2
# class People:
# 	name = 'Tom111'
# 	age = 18
# 	def __init__(self, n,a):
# 		self.name = n
# 		self.age =a

# 	def speak(self):
# 		print("%s is speaking: I am %d years old." %(self.name,self.age))

# p = People('hh',19)
# print(People.name)
# p.name = "Tom"
# p.speak()

#3、输入长方形的长和宽,计算长方形的面积.
# class Mji:
# 	def __init__(self,a,b):
# 		self.a = a
# 		self.b =b
# 	def cc(self):
# 		dd = self.a * self.b
# 		print(dd)
# aa = Mji(5,6)
# aa.cc()


# class Mji:
# 	a = 5
# 	b = 4
# 	def cc():
# 		dd = Mji.a * Mji.b
# 		print(dd)
# Mji.cc()

# class Mji:
# 	a = 3
# 	b = 3
# 	def cc(self):
# 		print(self.a * self.b)
# # h = Mji()
# # h.cc()
# Mji().cc()

# class Rectangle:
#     len = 10
#     wid = 5
#     area = len * wid
# juxing = Rectangle  #类的实例化 
# print(juxing.area)  #用实例去调用类中属性

#4、输入一个时间（以“小时:分钟:秒”的格式），输出该时间经过 5 分 30 秒后的时间。
# input_time = '23:54:30'
# time_list = input_time.split(':')
# # print(time_list)
# time_data = [int(x) for x in time_list]
# # print(time_data)
# # print(time_list)
# h = time_data[0]
# m = time_data[1]
# s = time_data[2]
# # print(s)
# # if((0<=h) and (h<=24) and (0<=m) and (m<=60) and (0<=s) and (s<=60)):
# if((0<=h<24) and (0<=m<=60) and (0<=s<=60)):
# 	s += 30
# 	if s>=60:
# 		s = s-60
# 		m+=1

# 	m += 5
# 	if m>=60:
# 		m = m-60
# 		h+=1

# 	if h == 24:
# 		h = 0
# 	print('经过5分30秒后的时间:%02d:%02d:%02d'%(h,m,s))

# else:
# 	print('你输入的时间格式有问题，请重新输入！')
# input_time = '23:54:11'
# h,m,s = input('您输入的时间（hh:mm:ss）:').split(':')
# # h,m,s = input_time.split(':')
# hh = int(h)
# mm = int(m)
# ss = int(s)

# if((0<=hh<24) and (0<=mm<=60) and (0<=ss<=60)):
# 	ss += 30
# 	# print(ss)
# 	mm += ss // 60
# 	ss = ss % 60
# 	# print(mm)

# 	mm += 5
# 	hh += mm //60
# 	mm = mm % 60

# 	hh = hh % 24
# 	print('经过5分30秒后的时间:%02d:%02d:%02d'%(hh,mm,ss))

# else:
# 	print('你输入的时间格式有问题，请重新输入！')

# import time
# def times(time_str):
# 	struce_time = time.strftime("%Y-%m-%d") + ' ' + time_str#元组转为字符串
# 	# print('元组转为字符串:'+struce_time)
# 	st = time.strptime(struce_time,"%Y-%m-%d %H:%M:%S")#字符串转为元组
# 	# print(st)
# 	mt = time.mktime(st)#元组转为时间戳
# 	# print(mt)
# 	wt = time.localtime(mt + 5*60 + 30)#时间戳转为元组
# 	# print(wt)
# 	tt = time.strftime("%H:%M:%S",wt)#元组转为字符串
# 	# return wt.tm_hour,wt.tm_min,wt.tm_sec
# 	return tt

# if __name__ == '__main__':  # if __name__ == '__main__' 中的 __name__ 代表的就是当前执行的模块名
# 	# input_time = '23:54:11'

# 	input_time = input('请输入时间hh:mm:ss：')
# 	aa = times(input_time)

# 	print(aa)


# # def get_month_timestamp(timestamp):
# #     #时间戳转为元组
# #     now = time.localtime(timestamp)
# #     #元组转为字符串
# #     monthStr = time.strftime("%Y-%m-00 00:00:00", now)
# #     #字符串转为元组
# #     monthTuple = time.strptime(monthStr, '%Y-%m-00 00:00:00')
# #     #元组转为时间戳
# #     return time.mktime(monthTuple)	

# 5、斐波那契数列为1,2,3,5,8,13,21.....根据这样的规律，编程求出400万以内最大的斐波那契数，并求出他是第几个斐波那契数。
# a, b = 1, 2
# count = 1
# while b < 4000000:
#     a, b = b, a + b
#     count += 1
# print(f'{a}是400万以内最大的斐波那契数，是第{count}个斐波那契数')

# s= 0
# for i in range(1,101):
# 	s += i
# 	print(i,s)