# -*- coding: utf-8 -*-
# @Author: somw
# @Date:   2020-10-15 23:41:50
# @Last Modified by:   somw
# @Last Modified time: 2020-10-15 23:48:55
number = [1,2,3,4,5,6,7,8]
o = []
j = []
while len(number)>0:
    numbers = number.pop()
    if numbers % 2 ==0:
        o.append(numbers)
    else:
        j.append(numbers)
print(o)
print(j)