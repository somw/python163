# -*- coding: utf-8 -*-
# @Author: somw
# @Date:   2020-10-15 00:10:59
# @Last Modified by:   somw
# @Last Modified time: 2020-10-15 00:16:20
year = input("请您输入年份：")
a = eval(year)
if a%400 == 0 or (a%4 == 0 and not a%100 == 0):
    print("闰年")
else:
    print("您输入的年份不是闰年")