# -*- coding: utf-8 -*-
# @Author: Administrator
# @Date:   2020-10-30 22:20:48
# @Last Modified by:   Administrator
# @Last Modified time: 2020-10-30 22:21:35
import json
import csv

def csv_json():
    json_fp = open('D:\\python\\sql作业附件\\airplanes.json', 'r')
    csv_fp = open('D:\\python\\sql作业附件\\airplanes.csv', 'w')
    data_list = json.load(json_fp)
    sheet_title = data_list[0].keys()
    sheet_data = []
    for data in data_list:
        sheet_data.append(data.values())
    writer = csv.writer(csv_fp)
    writer.writerow(sheet_title)
    writer.writerows(sheet_data)
    json_fp.close()
    csv_fp.close()

if __name__ == "__main__":
    csv_json()